#include "ntp_clock.hpp"

NTPClock::NTPClock()
{
  _udp = new WiFiUDP();
}

NTPClock::~NTPClock()
{
  _udp->stop();
}

void NTPClock::RequestTimeFromNTPServer(const char* host)
{
  byte packetBuffer[NTP_PACKET_SIZE];

  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  packetBuffer[0] = 0b11100011; // LI, Version, Mode
  packetBuffer[1] = 0;          // Stratum
  packetBuffer[2] = 6;          // Polling Interval
  packetBuffer[3] = 0xEC;       // Peer Clock Precision
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;

  Serial.printf("Getting time from NTP server %s\n", host);

  // start listening for response
  _udp->begin(_local_port);

  // send the request
  _udp->beginPacket(host, _ntp_port);
  _udp->write(packetBuffer, NTP_PACKET_SIZE);
  _udp->endPacket();
}

void NTPClock::ReadNTPResponse()
{
  byte incomingPacket[NTP_PACKET_SIZE];
  int packetSize = _udp->parsePacket();
  if (packetSize) {
    Serial.printf("Received %d bytes from %s, port %d\n", packetSize, _udp->remoteIP().toString().c_str(), _udp->remotePort());
    int len = _udp->read(incomingPacket, NTP_PACKET_SIZE);
    if ( len > 0 ) {
      unsigned long highWord = word(incomingPacket[40], incomingPacket[41]);
      unsigned long lowWord  = word(incomingPacket[42], incomingPacket[43]);
      _last_received_timestamp = highWord << 16 | lowWord;
      _last_timestamp_millis = millis();
    }
  }
}

unsigned long NTPClock::GetTimestamp()
{
  return _last_received_timestamp + (millis() - _last_timestamp_millis) / 1000;
}

unsigned long NTPClock::GetHour()
{
  return GetTimestamp() % (3600 * 24) / 3600;
}

unsigned long NTPClock::GetMinute()
{
  return GetTimestamp() % 3600 / 60;
}

unsigned long NTPClock::GetSecond()
{
  return GetTimestamp() % 60;
}

unsigned char* NTPClock::GetTimeFormatted()
{
  char time[] = "00:00:00";
  sprintf(time, "%2lu:%02lu:%02lu", GetHour(), GetMinute(), GetSecond());
  return time;
}
