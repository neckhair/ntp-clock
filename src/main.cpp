#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <ESP8266WiFi.h>

#include "ntp_clock.hpp"

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

#define OLED_RESET -1
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

NTPClock ntp_clock;

const char* ssid = "changeme";
const char* password = "changeme";

const char* NTP_HOST = "pool.ntp.org";

void setup() {
  Serial.begin(115200);
  Serial.println("Starting up...");

  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }

  display.clearDisplay();
  display.setTextColor(WHITE);

  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" connected");

  // Set time
  ntp_clock.RequestTimeFromNTPServer(NTP_HOST);
}

void loop() {
  ntp_clock.ReadNTPResponse();
  delay(1000);

  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0, 0);
  display.printf("%2lu:%02lu:%02lu", ntp_clock.GetHour(), ntp_clock.GetMinute(), ntp_clock.GetSecond());

  display.setTextSize(1);
  display.setCursor(0, 20);
  display.printf("TS: %lu", ntp_clock.GetTimestamp());
  display.display();
}
