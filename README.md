# NTP Clock for ESP8266

Shows the current time on an LCD display. The time is fetched
from an NTP server over the Wifi.
