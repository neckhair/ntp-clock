#ifndef NTPCLOCK_H
#define NTPCLOCK_H

#include <Arduino.h>
#include <WiFiUdp.h>

#define NTP_PACKET_SIZE 48

class NTPClock
{
private:
  int _ntp_port = 123;
  int _local_port = 8888;
  WiFiUDP* _udp;

  unsigned long _last_received_timestamp = 0;
  unsigned long _last_timestamp_millis = 0; // the millis() when the last timestamp was received

public:
  NTPClock();
  ~NTPClock();
  void RequestTimeFromNTPServer(const char* host);
  void ReadNTPResponse();
  unsigned long GetTimestamp();
  unsigned long GetHour();
  unsigned long GetMinute();
  unsigned long GetSecond();
};

#endif
